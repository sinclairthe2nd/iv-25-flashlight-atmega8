//
//Defines
#define F_CPU 2000000UL
//
//includes
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/interrupt.h>
//
//Setup
int main(void)
{ 
// Set Outputs and Inputs	
   DDRB = 0b11111111;

// define Variables
   uint16_t value;
   float voltage_fl;
   float conv = 0.0048828125;

// enable ADC
  ADCSRA |= (1<<ADEN);
  ADMUX |= ( (1<<REFS1) | (1<<REFS0) ); // internal reference
  ADMUX |= ((1 << MUX0) | (1 << MUX2)); // select ADC5

//8 bit PWM
DDRD   |= (1 << PD4);                   // PWM output on PD4
TCCR0 = ((1 << CS00) | (1 << CS01) |(0 << CS02));  //
TCNT0 = 0;

// Enable Interupts by using Dwarfen Magic
TIMSK |= (1 << TOIE0);
sei();         

//
//Main
while(1)
    {
    //ADC Reading
    ADCSRA |= (1<<ADSC);        // start reading
    while(ADCSRA & (1<<ADSC));  // wait until done

    // read result bytes (low and high)
    value  = ADCL;
    value |= (ADCH<<8);

    //Do some math to convert to Voltage
    voltage_fl = value*conv;    

    // Compare Value to Set value
    if (voltage_fl > 3.6) //All Segments
    {PORTB &= ~(1 << PB0);}
    else 
    {PORTB |= (1 << PB0);}

    if (voltage_fl > 3.4) // Six Segment
    {PORTB &= ~(1 << PB1);}
    else 
    {PORTB |= (1 << PB1);}
         
    if (voltage_fl > 3.2) // Five Segment
    {PORTB |= (1 << PB2);}
    else
    {PORTB &= ~(1 << PB2);}
     
    if (voltage_fl > 3) // Four Segment
    {PORTB |= (1 << PB3);}
    else
    {PORTB &= ~(1 << PB3);}
     
    if (voltage_fl > 2.8) // Three Segment
    {PORTB |= (1 << PB4);}
    else
    {PORTB &= ~(1 << PB4);}
     
    if (voltage_fl > 2.6) // Two Segment
    {PORTB |= (1 << PB5);}
    else
    {PORTB &= ~(1 << PB5);}
     
    if (voltage_fl > 2.5) // One Segment
    {PORTB |= (1 << PB6);}
    else
    {PORTB &= ~(1 << PB6);}
          
    if (voltage_fl < 2.4) // Blink last Segment
    {PORTB &= ~(1 << PB6); // PB3 goes low
     _delay_ms(150);
     PORTB |= (1 << PB6); // PB3 goes high
     _delay_ms(150);
     }
     else 
     {PORTB &= ~(1 << PB6);}



//the end is near :)
    }
}
//Interupts
ISR (TIMER0_OVF_vect)  // timer0 overflow interrupt
{
        PORTD ^= (1 << PD4);
}

